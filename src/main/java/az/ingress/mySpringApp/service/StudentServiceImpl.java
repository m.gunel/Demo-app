package az.ingress.mySpringApp.service;

import az.ingress.mySpringApp.dto.StudentRequestDto;
import az.ingress.mySpringApp.dto.StudentResponseDto;
import az.ingress.mySpringApp.model.Student;
import az.ingress.mySpringApp.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {


    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public int create(StudentRequestDto dto) {
        Student student = Student.builder()
                .name(dto.getName())
                .surname(dto.getSurname())
                .email(dto.getEmail())
                .build();
        studentRepository.save(student);
        return student.getId();
    }

    @Override
    public StudentResponseDto update(Integer id, StudentRequestDto dto) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));

        student.setName(dto.getName());
        student.setSurname(dto.getSurname());
        student.setEmail(dto.getEmail());

        studentRepository.save(student);

        return StudentResponseDto.builder()
                .name(student.getName())
                .surname(student.getSurname())
                .email(student.getEmail())
                .build();
    }

    @Override
    public void delete(Integer id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));

        studentRepository.delete(student);
    }


    @Override
    public StudentResponseDto get(Integer id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        return StudentResponseDto.builder()

                .name(student.getName())
                .surname(student.getSurname())
                .email(student.getEmail())
                .build();
    }

    @Override
    public List<StudentResponseDto> listAll() {
        List<Student> students = studentRepository.findAll();
        return students.stream()
                .map(student -> StudentResponseDto.builder()
                        .id(student.getId())
                        .name(student.getName())
                        .surname(student.getSurname())
                        .email(student.getEmail())
                        .build())
                .collect(Collectors.toList());    }

    @Override
    public List<Student> findAllByEmailContains(String s){
        List<Student> students = studentRepository.findAllByEmailContains(s);
        return students;

    }
}
