package az.ingress.mySpringApp.service;

import az.ingress.mySpringApp.dto.StudentRequestDto;
import az.ingress.mySpringApp.dto.StudentResponseDto;
import az.ingress.mySpringApp.model.Student;

import java.util.List;


public interface StudentService {
    int create(StudentRequestDto dto);

    StudentResponseDto update(Integer id,StudentRequestDto dto);

    void delete(Integer id);

    StudentResponseDto get(Integer id);

    List<StudentResponseDto> listAll();

    List<Student> findAllByEmailContains(String s);
}
