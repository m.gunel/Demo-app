package az.ingress.mySpringApp.service;

import az.ingress.mySpringApp.dto.ProductResponseDto;
import az.ingress.mySpringApp.dto.ShoppingCartResponseDto;
import az.ingress.mySpringApp.model.Category;
import az.ingress.mySpringApp.model.Product;
import az.ingress.mySpringApp.model.ShoppingCart;
import az.ingress.mySpringApp.repository.ProductRepository;
import az.ingress.mySpringApp.repository.ShoppingCartRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ShoppingCartServiceImpl implements ShoppingCartService{

    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    //private final CacheManager cacheManager;
    //private final RedisTemplate<Long, ShoppingCart> redisTemplate;

    public ShoppingCartServiceImpl(ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository, CacheManager cacheManager, RedisTemplate<Long, ShoppingCart> redisTemplate) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
        //this.redisTemplate = redisTemplate;
        //this.cacheManager = cacheManager;
    }

    @CachePut(cacheNames = "shoppingCarts", key = "#result.id")
    @Override
    public ShoppingCart createShoppingCart(String name) {

        ShoppingCart shoppingCart = ShoppingCart.builder()
                .name(name)
                .build();
        shoppingCart = shoppingCartRepository.save(shoppingCart);
        //redisTemplate.opsForValue().set(shoppingCart.getId(), shoppingCart);
        return shoppingCart;
    }
    @CacheEvict(cacheNames = "shoppingCarts",key = "#cartId")
    @Override
    public ShoppingCart addProductToCart(Long cartId,Integer productId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product not found"));
        ShoppingCart activeShoppingCart = shoppingCartRepository.findById(cartId)
                .orElseThrow(() -> new RuntimeException("ShoppingCart not found"));
        //redisTemplate.delete(cartId);
        activeShoppingCart.getProducts().add(product);
        return shoppingCartRepository.save(activeShoppingCart);
    }

    @CacheEvict(cacheNames = "shoppingCarts",key = "#cartId")
    @Override
    public ShoppingCart removeProductFromCart(Long cartId, Integer productId) {

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product not found"));
        ShoppingCart activeShoppingCart = shoppingCartRepository.findById(cartId)
                .orElseThrow(() -> new RuntimeException("ShoppingCart not found"));
        //redisTemplate.delete(cartId);
        activeShoppingCart.getProducts().remove(product);
        return shoppingCartRepository.save(activeShoppingCart);
    }

    @Cacheable(cacheNames = "shoppingCarts",key = "#cartId")
    @Override
    public ShoppingCart getShoppingCartById(Long cartId) {

        //ShoppingCart shoppingCart = (ShoppingCart) redisTemplate.opsForValue().get(cartId);
        //if(shoppingCart == null){
        ShoppingCart shoppingCart = shoppingCartRepository.findById(cartId)
                .orElseThrow(() -> new RuntimeException("ShoppingCart not found"));
            //log.info("Get cart from DB {}",cartId);
            //redisTemplate.opsForValue().set(cartId, shoppingCart);
       // }
//        else{
//            log.info("Get cart from Cache {}", cartId);
//        }
        return shoppingCart;
    }


}
