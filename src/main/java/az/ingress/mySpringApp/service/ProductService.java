package az.ingress.mySpringApp.service;

import az.ingress.mySpringApp.dto.ProductRequestDto;
import az.ingress.mySpringApp.dto.ProductResponseDto;
import az.ingress.mySpringApp.model.Category;

import java.util.List;
import java.util.Map;

public interface ProductService {
    int create(ProductRequestDto dto);

    public ProductResponseDto getProductById(Integer productId);

    List<ProductResponseDto> listAllByPrice(Integer min, Integer max);

    Map<Category, Long> countProductByCategory();

    public void delete(Integer id);
}
