package az.ingress.mySpringApp.service;

import az.ingress.mySpringApp.dto.ProductRequestDto;
import az.ingress.mySpringApp.dto.ProductResponseDto;
import az.ingress.mySpringApp.model.Category;
import az.ingress.mySpringApp.model.Product;
import az.ingress.mySpringApp.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public int create(ProductRequestDto dto) {

        Product product = Product.builder()
                .name(dto.getName())
                .price(dto.getPrice())
                .category(dto.getCategory())
                .subCategory(dto.getSubCategory())
                .productDetail(dto.getProductDetail())
                .description(dto.getDescription())
                .build();
        productRepository.save(product);
        return product.getId();
    }

    public ProductResponseDto getProductById(Integer productId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product not found"));

        ProductResponseDto responseDto = new ProductResponseDto();
        responseDto.setId(product.getId());
        responseDto.setName(product.getName());
        responseDto.setDescription(product.getDescription());
        responseDto.setSubCategory(product.getSubCategory());
        responseDto.setProductDetail(product.getProductDetail());
        return responseDto;
    }

    @Override
    public List<ProductResponseDto> listAllByPrice(Integer min, Integer max) {
        List<Product> products;
        if (min != null && max != null) {
            products = productRepository.findByPriceBetween(BigDecimal.valueOf(min), BigDecimal.valueOf(max));
        } else if (min != null) {
            products = productRepository.findByPriceGreaterThanEqual(BigDecimal.valueOf(min));
        } else if (max != null) {
            products = productRepository.findByPriceLessThanEqual(BigDecimal.valueOf(max));
        } else {
            products = productRepository.findAll();
        }
        return products.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Map<Category, Long> countProductByCategory() {
        return productRepository.countProductsByCategory()
                .stream()
                .collect(Collectors.toMap(
                        result -> (Category) result[0],
                        result -> (Long) result[1]
                ));
    }

    @Override
    public void delete(Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Product not found"));

        productRepository.delete(product);
    }


    private ProductResponseDto convertToDto(Product product) {
        return ProductResponseDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .category(product.getCategory())
                .description(product.getDescription())
                .build();
    }
}
