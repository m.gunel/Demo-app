package az.ingress.mySpringApp.service;

import az.ingress.mySpringApp.dto.ShoppingCartResponseDto;
import az.ingress.mySpringApp.model.ShoppingCart;

public interface ShoppingCartService {
    ShoppingCart  createShoppingCart(String name);

    ShoppingCart addProductToCart(Long cartId, Integer productId);
    ShoppingCart removeProductFromCart(Long cartId,Integer productId);

    ShoppingCart getShoppingCartById(Long cartId);


}
