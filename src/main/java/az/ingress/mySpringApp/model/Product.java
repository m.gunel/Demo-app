package az.ingress.mySpringApp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@NamedEntityGraph(name = "Product.detail",
        attributeNodes = {
                @NamedAttributeNode("subCategory"),
                @NamedAttributeNode("productDetail")
        })
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(unique = true, nullable = false)
    String name;

    @Column(nullable = false)
    BigDecimal price;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    Category category;

    @ManyToOne(fetch =FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "subcategory_id")
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    SubCategory subCategory;

    @OneToOne(fetch =FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "product_detail_id")
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    ProductDetail productDetail;
    
    String description;
}
