package az.ingress.mySpringApp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.apache.commons.lang3.builder.HashCodeExclude;
import org.apache.commons.lang3.builder.ToStringExclude;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String number;

    @ManyToOne
    @JoinColumn(name = "student_id")
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    Student student;
}
