package az.ingress.mySpringApp.model;

public enum Role {
    USER,
    ADMIN,
    MANAGER
}
