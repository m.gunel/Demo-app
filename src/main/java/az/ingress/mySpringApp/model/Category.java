package az.ingress.mySpringApp.model;

public enum Category {
    KITAB("KITAB"),
    ELEKTRONIKA("ELEKTRONIKA"),
    MEISET("MEISET");

    private final String categoryValue;

    Category(String categoryValue) {
        this.categoryValue = categoryValue;
    }

    public String getCategoryValue() {
        return categoryValue;
    }
}
