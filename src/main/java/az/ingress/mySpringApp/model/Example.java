package az.ingress.mySpringApp.model;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class Example {

    String name = "Example";


    @PostConstruct
    public void init(){
        log.info("Example bean initialized");
    }
    @PreDestroy
    public void destroy(){
        log.info("Example bean destroyed");
    }

}
