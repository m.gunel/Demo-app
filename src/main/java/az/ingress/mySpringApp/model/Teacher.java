package az.ingress.mySpringApp.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;

    String surname;
    Integer age;


    @OneToMany(cascade = CascadeType.ALL)     // onetomany'de 3cu cedvel elave etmekle
    @JoinTable(
            name = "teacher_phone",
            joinColumns = @JoinColumn(name="teacher_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "phone_id",referencedColumnName = "id")
    )
    Set<Phone> phones = new HashSet<>();   // null olmasin deye
}
