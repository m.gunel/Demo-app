package az.ingress.mySpringApp.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Entity
    public class ProductDetail implements Serializable {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        Integer id;

        @Column(nullable = false)
        String color;

        @Column(nullable = false)
        String imageUrl;
    }


