package az.ingress.mySpringApp.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "shopping_carts")
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = -4241013755392182564L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "shopping_carts_products",
            joinColumns = @JoinColumn(name = "shopping_cart_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "product_id",referencedColumnName = "id")
    )
    List<Product> products = new ArrayList<>();

}
