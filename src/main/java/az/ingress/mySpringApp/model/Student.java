package az.ingress.mySpringApp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.ToStringExclude;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@NamedEntityGraph(name = "Student.additional",
                  attributeNodes = {
                    @NamedAttributeNode("address"),
                    @NamedAttributeNode("phones")
                  }
)
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;

    String surname;
    String email;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    Address address;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student",fetch = FetchType.EAGER)
    Set<Phone> phones = new HashSet<>();   // null olmasin deye
}
