package az.ingress.mySpringApp.dto;
import az.ingress.mySpringApp.model.Category;
import az.ingress.mySpringApp.model.ProductDetail;
import az.ingress.mySpringApp.model.SubCategory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductResponseDto {

    Integer id;
    String name;
    BigDecimal price;
    Category category;
    SubCategory subCategory;
    ProductDetail productDetail;
    String description;
}
