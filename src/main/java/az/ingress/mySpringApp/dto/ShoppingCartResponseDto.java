package az.ingress.mySpringApp.dto;
import az.ingress.mySpringApp.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShoppingCartResponseDto {

    Long id;
    String name;
    List<Product> products = new ArrayList<>();
}
