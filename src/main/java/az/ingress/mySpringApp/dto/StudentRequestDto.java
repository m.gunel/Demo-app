package az.ingress.mySpringApp.dto;

import lombok.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentRequestDto{

    String name;
    String surname;
    String email;


}
