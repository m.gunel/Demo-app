package az.ingress.mySpringApp.dto;

import az.ingress.mySpringApp.model.Category;
import az.ingress.mySpringApp.model.Product;
import az.ingress.mySpringApp.model.ProductDetail;
import az.ingress.mySpringApp.model.SubCategory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShoppingCartRequestDto {

    String name;
    List<Product> products = new ArrayList<>();


}
