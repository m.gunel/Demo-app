package az.ingress.mySpringApp.repository;

import az.ingress.mySpringApp.model.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Integer> {
    @Override          // namedENTITYGRAF
    @EntityGraph(value = "Student.additional",type = EntityGraph.EntityGraphType.LOAD)
    List<Student> findAll();


    List<Student> findAllByEmailContains(String s);
}
