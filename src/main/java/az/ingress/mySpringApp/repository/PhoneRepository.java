package az.ingress.mySpringApp.repository;

import az.ingress.mySpringApp.model.Phone;
import az.ingress.mySpringApp.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhoneRepository extends JpaRepository<Phone,Integer> {

}
