package az.ingress.mySpringApp.repository;

import az.ingress.mySpringApp.model.Address;
import az.ingress.mySpringApp.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address,Integer> {


}
