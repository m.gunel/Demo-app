package az.ingress.mySpringApp.repository;

import az.ingress.mySpringApp.model.Category;
import az.ingress.mySpringApp.model.Product;
import az.ingress.mySpringApp.model.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {
    @Override
    @EntityGraph(value = "Product.detail", type = EntityGraph.EntityGraphType.LOAD)
    Optional<Product> findById(Integer id);

    List<Product> findByPriceBetween(BigDecimal priceFrom, BigDecimal priceTo);

    List<Product> findByPriceGreaterThanEqual(BigDecimal price);

    List<Product> findByPriceLessThanEqual(BigDecimal price);

    @Query(value = "SELECT p.category, COUNT(p) FROM Product p GROUP BY p.category", nativeQuery = true)
    List<Object[]> countProductsByCategory();
}
