package az.ingress.mySpringApp.repository;

import az.ingress.mySpringApp.model.Authority;
import az.ingress.mySpringApp.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority,Integer> {

}
