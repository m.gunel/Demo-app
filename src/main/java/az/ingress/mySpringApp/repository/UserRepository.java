package az.ingress.mySpringApp.repository;

import az.ingress.mySpringApp.model.Phone;
import az.ingress.mySpringApp.model.Student;
import az.ingress.mySpringApp.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Integer> {

//    @Override          // ENTITYGRAF
//    @EntityGraph(attributePaths = "authorities")  // to solve n+1 problem EAger
//    List<User> findAll();




}
