package az.ingress.mySpringApp.repository;

import az.ingress.mySpringApp.model.Phone;
import az.ingress.mySpringApp.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher,Integer> {

}
