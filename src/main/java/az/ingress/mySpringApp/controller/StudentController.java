package az.ingress.mySpringApp.controller;

import az.ingress.mySpringApp.dto.StudentRequestDto;
import az.ingress.mySpringApp.dto.StudentResponseDto;
import az.ingress.mySpringApp.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URL;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    private StudentService studentService;
    @Autowired
    public void setStudentServiceService(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<StudentResponseDto> get (@PathVariable Integer id) {

        StudentResponseDto res = studentService.get(id);
        return ResponseEntity.ok(res);
    }
    @PostMapping
    public ResponseEntity<Void> create (@RequestBody StudentRequestDto dto) {

        int id = studentService.create(dto);
        return ResponseEntity.created(URI.create("student/" + id)).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<StudentResponseDto> update (@PathVariable Integer id,
                                                      @RequestBody StudentRequestDto dto) {

        StudentResponseDto response = studentService.update(id,dto);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete (@PathVariable Integer id) {

        studentService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<StudentResponseDto>> listAll() {
        List<StudentResponseDto> students = studentService.listAll();
        return ResponseEntity.ok(students);
    }

}
