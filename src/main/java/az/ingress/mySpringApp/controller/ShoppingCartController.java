package az.ingress.mySpringApp.controller;

import az.ingress.mySpringApp.dto.ProductResponseDto;
import az.ingress.mySpringApp.dto.ShoppingCartRequestDto;
import az.ingress.mySpringApp.dto.ShoppingCartResponseDto;
import az.ingress.mySpringApp.model.ShoppingCart;
import az.ingress.mySpringApp.service.ShoppingCartService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/shopping-carts")
public class ShoppingCartController {

    private final ShoppingCartService shoppingCartService;

    public ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @PostMapping
    public ResponseEntity<ShoppingCart> createShoppingCart(@RequestBody Map<String,String> request) {
        String name = request.get("name");
        ShoppingCart shoppingCart = shoppingCartService.createShoppingCart(name);
        return new ResponseEntity<>(shoppingCart,HttpStatus.CREATED);
    }

    @PostMapping("/{cartId}/product")
    public ResponseEntity<ShoppingCart> addProductToCart(@PathVariable Long cartId, @RequestBody Map<String, Integer> request) {
        Integer productId = request.get("productId");
        ShoppingCart shoppingCart = shoppingCartService.addProductToCart(cartId, productId);
        return ResponseEntity.ok(shoppingCart);
    }

    @DeleteMapping("/{cartId}/product/{productId}")
    public ResponseEntity<ShoppingCart> removeProductFromCart(@PathVariable Long cartId, @PathVariable Integer productId) {
        ShoppingCart shoppingCart = shoppingCartService.removeProductFromCart(cartId, productId);
        return ResponseEntity.ok(shoppingCart);
    }


    @GetMapping("/{cartId}")
    public ResponseEntity<ShoppingCart> getShoppingCartById(@PathVariable Long cartId) {
        ShoppingCart responseDto = shoppingCartService.getShoppingCartById(cartId);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
