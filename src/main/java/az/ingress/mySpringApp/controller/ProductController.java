package az.ingress.mySpringApp.controller;

import az.ingress.mySpringApp.dto.ProductRequestDto;
import az.ingress.mySpringApp.dto.ProductResponseDto;
import az.ingress.mySpringApp.model.Category;
import az.ingress.mySpringApp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/product")
public class ProductController {
    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody ProductRequestDto dto) {
        int id = productService.create(dto);
        return ResponseEntity.created(URI.create("product/" + id)).build();
    }

    @GetMapping
    public ResponseEntity<List<ProductResponseDto>> listAllByPrice(
            @RequestParam(required = false) Integer min,
            @RequestParam(required = false) Integer max) {
        List<ProductResponseDto> products = productService.listAllByPrice(min, max);
        return ResponseEntity.ok(products);
    }

    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable Integer id) {
        productService.delete(id);

    }


    @GetMapping("/countByCategory")
    public ResponseEntity<Map<Category, Long>> countProductByCategory() {
        Map<Category, Long> countByCategory = productService.countProductByCategory();
        return ResponseEntity.ok(countByCategory);
    }
}
