package az.ingress.mySpringApp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
@Slf4j
public class HelloController {


   @GetMapping("/hello2")
   public String hello() {
      return "Hello from 2";

   }
}
