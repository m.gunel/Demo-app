package az.ingress.mySpringApp.config;

import az.ingress.mySpringApp.model.User;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "my-app")
@Data
public class ConfigurationProp {
    private Integer appVersion;
    private List<String> myList;
    private List<User> users;
    private Map<String,String> map;
}
