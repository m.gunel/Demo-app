package az.ingress.mySpringApp.config;

import az.ingress.mySpringApp.model.Example;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    public Example getExample(){
        return  new Example();
    }


}
