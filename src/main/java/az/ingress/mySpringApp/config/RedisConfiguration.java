package az.ingress.mySpringApp.config;

import az.ingress.mySpringApp.model.ShoppingCart;
import az.ingress.mySpringApp.model.User;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;

import java.time.Duration;
import java.util.List;
import java.util.Map;

@Configuration
public class RedisConfiguration {
    @Value("${spring.data.redis.host}")
    private String host;
    @Value("${spring.data.redis.port}")
    private Integer port;

    @Bean
    public LettuceConnectionFactory redisConnectionFactory(){
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(host);
        configuration.setPort(port);
        return new LettuceConnectionFactory(configuration);
    }
    @Bean
    public CacheManager cacheManager (){
        RedisCacheConfiguration days3 = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofDays(3));
        RedisCacheConfiguration month1 = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofDays(30));
        return RedisCacheManager.builder(redisConnectionFactory())
                .cacheDefaults(month1)
                .withCacheConfiguration("shoppingCart",days3)
                .build();
    }


    @Bean
    public RedisTemplate<Long, ShoppingCart> redisTemplate(){
          final RedisTemplate<Long, ShoppingCart> template = new RedisTemplate<>();
          template.setConnectionFactory(redisConnectionFactory());

        var jacksonRedisSerializer =  new GenericJackson2JsonRedisSerializer();
        template.setValueSerializer(jacksonRedisSerializer);
        template.setKeySerializer(jacksonRedisSerializer);
        template.afterPropertiesSet();
        return template;

    }


}
