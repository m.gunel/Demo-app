package az.ingress.mySpringApp;

import az.ingress.mySpringApp.config.ConfigurationProp;
import az.ingress.mySpringApp.controller.ProductController;
import az.ingress.mySpringApp.model.*;
import az.ingress.mySpringApp.repository.*;
import az.ingress.mySpringApp.service.StudentService;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.domain.Specification;

import java.math.BigDecimal;
import java.util.*;


@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
@EnableCaching
public class MySpringAppApplication implements CommandLineRunner {
	private final ShoppingCartRepository shoppingCartRepository;
	private final TeacherRepository teacherRepository;

	@Value("${my-app.app-version}")
	private Integer appVersion;

	@Value("${asan.url}")
	private String asanUrl;

	private final ConfigurationProp prop;

	private final EntityManagerFactory emf;

	private final StudentService studentService;
	private final StudentRepository studentRepository;
	private final AddressRepository addressRepository;
	private final PhoneRepository phoneRepository;
	private final UserRepository userRepository;
	private final AuthorityRepository authorityRepository;
	private final ProductRepository productRepository;

	public static void main(String[] args) {

		ConfigurableApplicationContext context = SpringApplication.run(MySpringAppApplication.class, args);
        ProductController productController = context.getBean(ProductController.class);


//		ProductDetail productDetail = new ProductDetail();
//		productDetail.setColor("Blue");
//		productDetail.setImageUrl("http://example.com/image.jpg");
//
//		SubCategory subCategory = new SubCategory();
//		subCategory.setName("Book2");

//		ProductRequestDto newProductDto = new ProductRequestDto("Book X2", BigDecimal.valueOf(19.99), Category.KITAB, subCategory,productDetail,"Description for Book X");
//		productController.deleteProduct(28);
//		ResponseEntity<Void> createResponse = productController.create(newProductDto);
//		System.out.println("Created product with status code: " + createResponse.getStatusCode());
//
//		ResponseEntity<Object[]> listResponse = new RestTemplate().getForEntity("http://localhost:8081/my-app1/product?min=50&max=100", Object[].class);
//		System.out.println("Listed products with status code: " + listResponse.getStatusCode());
//		System.out.println("Products: " + Arrays.toString(listResponse.getBody()));
//
//
//		ResponseEntity<Object[]> listResponse2 = new RestTemplate().getForEntity("http://localhost:8081/my-app1/product?min=50", Object[].class);
//		System.out.println("Listed products with status code: " + listResponse2.getStatusCode());
//		System.out.println("Products: " + Arrays.toString(listResponse2.getBody()));
//
//		ResponseEntity<Map<Category, Long>> countResponse = productController.countProductByCategory();
//		System.out.println("Counted products by category with status code: " + countResponse.getStatusCode());
//		System.out.println("Product counts by category: " + countResponse.getBody());

		//context.close();

	}


	@Override
	public void run(String... args) throws Exception {

//		Specification<Product> specification = ((root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("price"),6000.0));
//		Specification<Product> specification2 = ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("name"),"TV"));
//		Specification<Product> specification3 = (root, query, criteriaBuilder) -> {
//			Join<Product,SubCategory> join =root.join("subCategory", JoinType.INNER);
//			return criteriaBuilder.equal(join.get("name"),"LED");
//
//		};
//
//		List<Product> all = productRepository.findAll(specification3);
//		System.out.println(all);




////lesson11
//		//JDBC
////		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","12345")) {
////			Statement statement = conn.createStatement();
////			ResultSet rs = statement.executeQuery("select * from student");
////			while(rs.next()){
////				System.out.println(rs.getInt(1));
////				System.out.println(rs.getString(2));
////				System.out.println(rs.getString(3));
////				System.out.println(rs.getString(4));
////			}
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
//
//
//		//JPA
////		EntityManager em = emf.createEntityManager();
////
////		List<StudentResponseDto> resultList = em.createNativeQuery("select * from student /*where id > :id*/", StudentResponseDto.class )
////				//.setParameter("id",1)
//////				.setFirstResult(1)
//////				.setMaxResults(2)
////				.getResultList();
////		System.out.println(resultList);
////
////		em.getTransaction().begin();
////
////		int r = em.createNativeQuery("update student set name = :name where id = :id")
////						.setParameter("name","Murad")
////				        .setParameter("id",2L)
////								.executeUpdate();
////
////		System.out.println("Effected rows -> " + r);
////
////		em.getTransaction().commit();
////
////		em.close();
//
//
//
//		//HIBERNATE   ORM
//
////		Session session = emf.createEntityManager().unwrap(Session.class);
////		Student student = session.get(Student.class,1L);
////		System.out.println(student);
////		List<StudentResponseDto> studentResponseDto = session.createNativeQuery("select * from student", StudentResponseDto.class)
////				.getResultList();
////		System.out.println(studentResponseDto);
////		//session.getTransaction().begin();
////		//session.getTransaction().commit();
////		session.close();
//
//
//        //HQL
//
////		EntityManager em = emf.createEntityManager();
////		List<Student> students = em.createQuery("select s from Student s where s.id > :id", Student.class)
////				.setParameter("id",2L)
////						.getResultList();
////
////		System.out.println(students);
////		em.close();
//
//
//		//REPOSITORY
//
////		List<StudentResponseDto> s = studentService.listAll();
////		System.out.println(s);
//
////		List<Student> s = studentService.findAllByEmailContains("qqq");
////		System.out.println(s);
//
////lesson 12 one to one / one to many
//
//		//createStudent();
//		//Student student = studentRepository.findById(21).get();
//
//
//		//studentRepository.save(student);
//		//studentRepository.deleteById(20);
//
//        // studentRepository.deleteById(17);
////		Student student = studentRepository.findById(15).get();
////		student.setAddress(null);
////		studentRepository.save(student);
////
////		addressRepository.deleteById(5);
//
//
//
////		Student student = studentRepository.findById(13).get();
////	log.info("Student is " + student);
//	//	studentRepository.deleteById(13);
//
//
//	// lesson 13 many to many
////        authorityRepository.save(Authority.builder()
////						.role(Role.ADMIN)
////				.build());
////		authorityRepository.save(Authority.builder()
////				.role(Role.USER)
////				.build());
////		authorityRepository.save(Authority.builder()
////				.role(Role.MANAGER)
////				.build());
//
//
////createuser();
//		//userRepository.deleteById(2);
//		//User user = userRepository.findById(3).get();
//		//System.out.println(user.getAuthorities());
//		//user.getAuthorities().removeIf((authority) -> authority.getRole().equals(Role.USER));
//        //userRepository.save(user);
//
////		Student student = studentRepository.findById(21).get();
////		System.out.println(student.getAddress());
////		System.out.println(student.getPhones());
//
//		//List<User> all = userRepository.findAll();
//		//System.out.println(all);
//
//		//List<Student> all = studentRepository.findAll();
//		//System.out.println(all);
//
//
////		Phone phone = Phone.builder()
////				.number("0556666666")
////				.build();
////        Teacher teacher = Teacher.builder()
////				.name("Arif")
////				.surname("Memmedli")
////				.age(52)
////				.phones(Set.of(phone))
////				.build();
////
////		teacherRepository.save(teacher);
//
//
//		// task Shopping Card
//
//		//createProduct();
//
//		//Product product1 = productRepository.getReferenceById(1);
//		//Product product2 = productRepository.getReferenceById(2);
////		Product product3 = productRepository.getReferenceById(2);
////
////		ShoppingCart shoppingCart = ShoppingCart.builder()
////				.name("basket2")
////				.products(List.of(product3))
////				.build();
////
////		shoppingCartRepository.save(shoppingCart);
//
//
//
   }
private void createuser(){

		Authority role_admin = authorityRepository.getReferenceById(1);
		Authority role_user = authorityRepository.getReferenceById(2);
		User user = User.builder()
				.username("gunel")
				.password("password")
				.authorities(Set.of(role_user,role_admin))
				.build();

		userRepository.save(user);
}
	private void createStudent() {

				Address address = Address.builder()
				.city("Baku")
				.country("Azerbaijan")
				.street("ehmed cemil 36")
		.build();

		Student student = Student.builder()
				.email("kazim@mail.ru")
				.name("kazim")
				.surname("mustafa")
				.address(address)
				.build();

		Phone phone1 = Phone.builder()
				.number("0556666666")
				.student(student)
				.build();

		Phone phone2 = Phone.builder()
				.number("0557777777")
				.student(student)
				.build();

		Set<Phone> phones = new HashSet<>();
		phones.add(phone1);
		phones.add(phone2);

		student.setPhones(phones);
		studentRepository.save(student);
	}

	private void createProduct(){
        ProductDetail productDetail =ProductDetail.builder()
				.color("black")
				.imageUrl("https://en.wikipedia.org/wiki/tv")
				.build();

		SubCategory subCategory = SubCategory.builder()
				.name("LED")
				.build();

		Product product = Product.builder()
				.name("TV")
				.price(BigDecimal.valueOf(5000.0))
				.description("Ref")
				.category(Category.ELEKTRONIKA)
				.productDetail(productDetail)
				.subCategory(subCategory)
				.build();

		productRepository.save(product);
	}
}
